import firebase from 'react-native-firebase';
import { Alert } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';

export function UploadImage(url, id) {
    var storageRef = firebase.storage().ref().child(id);
    storageRef.putFile(url);
}

export function DownloadImage(id, cb) {
    var storageRef = firebase.storage().ref().child(id);
    storageRef.getDownloadURL()
        .then((url) => {
            cb(url);
        })
        .catch(function (error) {
            switch (error.code) {
                case 'storage/object-not-found':
                    //ToastAndroid.show('object not found', ToastAndroid.LONG);
                    break;

                case 'storage/unauthorized':
                    Alert.alert('Permission denided')
                    break;

                case 'storage/canceled':
                    break;

                case 'storage/unknown':
                    Alert.alert('Uknown error');
                    break;
            }
        });
}