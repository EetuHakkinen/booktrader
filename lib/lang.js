import english from '../constants/lang_eng';
import finnish from '../constants/lang_fin';
import { Language } from './dbHandler';
var currLang = 'fi';

//currLang = Language();

export default l = (key) => {
    var s;
    switch (currLang) {
        case "fi":
            s = finnish[key];
            break;
        default:
            s = english[key];
            break;
    }
    return s;
}