import firebase from 'react-native-firebase';
import { Alert } from 'react-native';
import { Uid } from './auth';
import { UploadImage } from './storage';

export function ListenBookList(cbList) {
    firebase.database().ref('/forSale').on('value', v => {
        var list = [];
        var books = v.val();
        for (var b in books) {
            if (books.hasOwnProperty(b)) {
                list.push({
                    key: b,
                    info: books[b].info,
                    name: books[b].name,
                    price: books[b].price,
                    quality: books[b].quality,
                    seller: books[b].seller
                })
            }
        }
        cbList(list);
    })
}

export function ListenAdmin(cb) {
    firebase.database.ref('/').child('admin').on('value', v => {
        cb(v.val());
    })
}

export function ListenUsername(cb) {
    Uid().then(v => {
        var ref = firebase.database().ref('/users/' + v);
        ref.child('name').on('value', v => {
            cb(v.val());
        })
    });
}

export function CreateUser(email, password, name, phone) {
    if (email !== '' && password !== '') {
        firebase.auth().createUserWithEmailAndPassword(email.trim(), password)
            .then((user) => {
                var userRef = firebase.database().ref('/users/' + user.user.uid);
                userRef.set({
                    name: name,
                    phone: phone,
                    email: email,
                    created: Date.now()
                });
            })
            .catch(e => {
                Alert.alert(e.code);
            })
    }
}

export async function AddSell(name, qty, price, info, url) {
    var uid = await Uid();
    var ref = firebase.database().ref('/forSale');
    var userRef = firebase.database().ref('/users/' + uid)
    var id = RandomID();
    if (name !== '' && price !== ''){
        ref.child(id).once('value').then(v => {
            while (!v.val()) {
                ref.update({
                    [id]: {
                        name: name,
                        date: Date.now(),
                        quality: qty,
                        price: price,
                        info: info,
                        seller: uid
                    }
                })
                userRef.child('booksForSale').update({
                    [id]: true
                })
                UploadImage(url, id);
            }
        })
    }
}

function RandomID() {
    var n = '';
    for (var i = 0; i < 10; i++) {
        n = n + Math.floor(Math.random() * 10).toString();
    }
    return n;
}

export async function Language() {
    var uid = await Uid();
    var userRef = firebase.database().ref('/users/' + uid);
    return userRef.child('lang').on('value', v => {
        return v.val();
    });
}

export async function OwnBooks(cb) {
    var u = await Uid();
    var userRef = firebase.database().ref('/users/' + u);
    var forSaleRef = firebase.database().ref('/forSale');
    userRef.child('booksForSale').on('value', async function (v) {
        var books = v.val();
        var ownBooks = [];
        for (var b in books) {
            var c = await forSaleRef.child(b).once('value');
            var ownBooksObj = c.val();
            var objToPush = {
                key: b,
                info: ownBooksObj.info,
                name: ownBooksObj.name,
                price: ownBooksObj.price,
                quality: ownBooksObj.quality,
            }
            ownBooks.push(objToPush);
        }
        if (ownBooks.length > 0) {
            cb(ownBooks);
        } else {
            cb('no-books');
        }
    });
}

export async function CheckUser(cb) {
    var uid = await Uid();
    var userRef = firebase.database().ref('/users/' + uid);
    userRef.child('created').once('value').then(v => {
        cb(v.val());
    })
}

export function BookInformation(bookId, cb, name, number, email, u) {
    var bookInfoRef = firebase.database().ref('/forSale');
    bookInfoRef.child(bookId).once('value').then(v => {
        cb(v.val());
        return v.val().seller;
    }).then((uid) => {
        var sellerRef = firebase.database().ref('/users/');
        sellerRef.child(uid).on('value', v => {
            var i = v.val();
            name(i.name);
            number(i.phone);
            email(i.email);
            u(uid);
        });
    });

}

export function UserInfo(cb) {
    Uid().then(u => {
        var userRef = firebase.database().ref('/users/' + u);
        userRef.on('value', v => {
            cb(v.val().name, v.val().phone, v.val().email);
        });
    });
}

export function ListenMetadata(superuser, metadata) {
    var ref = firebase.database().ref('/metadata/');
    ref.child('superusers').on('value', v => {
        superuser(v.val().u);
    });
    ref.child('denyAll').on('value', v => {
        metadata(v.val());
    });
}

export async function Remove(id) {
    var uid = await Uid();
    var userRef = firebase.database().ref('/users/' + uid + '/booksForSale');
    userRef.child(id).remove();

    var forSaleRef = firebase.database().ref('/forSale/')
    forSaleRef.child(id).remove();
}

export async function UpdateUser(name, phone, email) {
    var uid = await Uid();
    var userRef = firebase.database().ref('/users/').child(uid);
    userRef.update({
        name,
        phone,
        email
    })
}