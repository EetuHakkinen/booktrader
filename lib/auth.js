import firebase from 'react-native-firebase';

export function User(cb) {
    firebase.auth().onAuthStateChanged((user) => {
        cb(user);
    })
}

export async function Uid() {
    return new Promise(function(resolve, reject) {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                resolve(user.uid);
            }
        })
    })
}

export function SignOut() {
    firebase.auth().signOut();
}