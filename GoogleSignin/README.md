
# react-native-google-signin

## Getting started

`$ npm install react-native-google-signin --save`

### Mostly automatic installation

`$ react-native link react-native-google-signin`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-google-signin` and add `RNGoogleSignin.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNGoogleSignin.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNGoogleSigninPackage;` to the imports at the top of the file
  - Add `new RNGoogleSigninPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-google-signin'
  	project(':react-native-google-signin').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-google-signin/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-google-signin')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNGoogleSignin.sln` in `node_modules/react-native-google-signin/windows/RNGoogleSignin.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Google.Signin.RNGoogleSignin;` to the usings at the top of the file
  - Add `new RNGoogleSigninPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNGoogleSignin from 'react-native-google-signin';

// TODO: What to do with the module?
RNGoogleSignin;
```
  