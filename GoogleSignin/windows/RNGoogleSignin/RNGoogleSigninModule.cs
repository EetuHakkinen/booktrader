using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Google.Signin.RNGoogleSignin
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNGoogleSigninModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNGoogleSigninModule"/>.
        /// </summary>
        internal RNGoogleSigninModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNGoogleSignin";
            }
        }
    }
}
