import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ToastAndroid } from 'react-native';
import firebase from 'react-native-firebase';

import Login from './components/Login';
import { CheckUser, ListenMetadata } from './lib/dbHandler';
import CreateUser from './components/CreateUser';
import LoadingSpinner from './components/LoadingSpinner';
import Profile from './components/Profile';
import Home from './components/Home';
import Sell from './components/Sell';
import { User, Uid } from './lib/auth';
import { createMaterialTopTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            user: null,
            userCreated: true,
            loading: true,
            metadata: false,
            superuser: false,
        }
    }
    componentDidMount() {
        User(u => this.setState({ user: u, loading: false }));
        ListenMetadata(s => this.setState({ superuser: s }), m => this.setState({ metadata: m }))
    }

    render() {
        if (this.state.metadata && !this.state.superuser){
            return (
                <Text>Sovelluksessa on huoltokatko, yritä myöhemmin uudestaan!</Text>
            );
        }

        if (this.state.loading){
            return <LoadingSpinner />
        }

        if (this.state.user){
            return (
                <Navigator />
            );
        }
        
        return <Login />
    }
}

const Navigator = createMaterialTopTabNavigator({
    Home: { 
        screen: Home, 
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Icon name="home" color={tintColor} size={30} />
            )
        }},
    Profile: { 
        screen: Profile,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Icon name="person" color={tintColor} size={30} />
            )
        }
    },
    Sell: { 
        screen: Sell,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Icon name="attach-money" color={tintColor} size={30} />
            )
        }
    }
},{
    initialRouteName: 'Home',
    order: ['Profile', 'Home', 'Sell'],
    tabBarPosition: 'bottom',
    tabBarOptions: {
        activeTintColor: '#00FF66',
        inactiveTintColor: 'grey',
        style: {
            backgroundColor: '#f2f2f2',
        },
        indicatorStyle: {
            height: 0
        },
        showIcon: true,
        showLabel: false,
    }
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
