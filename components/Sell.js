import React, { Component } from 'react';
import { StyleSheet, Dimensions, Text, View, TextInput, ScrollView, BackHandler, Image, CameraRoll, FlatList, TouchableHighlight, ToastAndroid, Button } from 'react-native';
import { AddSell, OwnBooks } from '../lib/dbHandler';
import StarRating from 'react-native-star-rating';
import l from '../lib/lang';
import Booklist from './Booklist';
import BookStats from './BookStats';
import ImagePicker from 'react-native-image-crop-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { UploadImage } from '../lib/storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { FloatingAction } from 'react-native-floating-action';
import AddIcon from '../images/add.png';

export default class Sell extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 'this',
            ownBooks: [],
            toBook: '',
        }
        this.toBookStats = this.toBookStats.bind(this);
    }

    componentWillMount() {
        if (this.props.booklist) {
            this.setState({ ownBooks: this.props.booklist });
        }
    }

    componentDidMount() {
        OwnBooks(b => this.setState({ ownBooks: b }));
    }

    toBookStats() {
        this.setState({ page: 'stats' });
    }

    render() {
        if (this.state.page === 'stats') {
            return <Stats booklist={this.state.ownBooks} />
        }
        if (this.state.toBook !== '') {
            return <BookStats bookId={this.state.toBook} where="Sell" />
        }

        const actions = [{
            text: 'Add',
            icon: AddIcon,
            name: 'Add',
            position: 1
        }];

        return (
            <View>
                <Button
                    title={l('sellBook')}
                    onPress={this.toBookStats}
                    color="#00FF66"
                />
                {this.state.ownBooks === 'no-books'
                    ? <Text>Sinulla ei ole vielä yhtään kirjaa myynnissä.</Text>
                    : <Booklist booklist={this.state.ownBooks} toBook={b => this.setState({ toBook: b })} />}
            </View>
        );
    }
}

class Stats extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 'this',
            name: '',
            price: '',
            info: '',
            url: '',
            starCount: 0,
            cam: false,
            photos: [],
            avatarSource: null,
        }
        this.toSellPage = this.toSellPage.bind(this);
        this.sell = this.sell.bind(this);
        this.onStarRatingPress = this.onStarRatingPress.bind(this);
        this.openPicker = this.openPicker.bind(this);
        this.takePicture = this.takePicture.bind(this);
    }

    componentWillMount() {
        this.backhandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.setState({ page: 'sell' });
            return true
        })
    }

    componentWillUnmount() {
        this.backhandler.remove();
    }

    toSellPage() {
        this.setState({ page: 'sell' })
    }

    sell() {
        if (this.state.name !== '' && this.state.starCount !== 0 && this.state.price !== '') {
            AddSell(this.state.name, this.state.starCount, this.state.price, this.state.info, this.state.url);
            this.toSellPage();
        }
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    openPicker() {
        const Blob = RNFetchBlob.polyfill.Blob;
        const fs = RNFetchBlob.fs;
        window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
        window.Blob = Blob;
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            mediaType: 'photo'
        }).then(image => {
            const imagePath = image.path;
            this.setState({ url: imagePath });
        });
    }

    takePicture() {
        const Blob = RNFetchBlob.polyfill.Blob;
        const fs = RNFetchBlob.fs;
        window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
        window.Blob = Blob;
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            const imagePath = image.path;
            this.setState({ url: imagePath });
        });
    }

    render() {
        if (this.state.page === 'sell') {
            return <Sell booklist={this.props.booklist} />
        }

        return (
            <ScrollView>
                <View style={styles.content}>
                    <Text style={{ fontSize: 25 }}>Lisää myynti-ilmoitus</Text>
                    <Text>{l('nameOfBook')}</Text>
                    <TextInput
                        style={{ height: 40 }}
                        placeholder={l('nameOfBook')}
                        onChangeText={(text) => this.setState({ name: text })}
                    />

                    {this.state.url ? <Image source={{ uri: this.state.url }} style={{ height: Dimensions.get('window').width - 20, width: Dimensions.get('window').width - 20 }} /> : <View />}

                    <View style={styles.buttons}>
                        <Button
                            title="valitse kuva"
                            onPress={this.openPicker}
                        />
                        <Button
                            title="ota kuva"
                            onPress={this.takePicture}
                        />
                    </View>


                    <Text>{l('price')}</Text>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TextInput
                            style={{ height: 40 }}
                            placeholder={l('price')}
                            onChangeText={(text) => this.setState({ price: text })}
                        />
                    </View>


                    <Text>{l('quality')}</Text>
                    <View style={styles.stars}>
                        <StarRating
                            disabled={false}
                            maxStars={5}
                            size={10}
                            rating={this.state.starCount}
                            selectedStar={(rating) => this.onStarRatingPress(rating)}
                            fullStarColor="#00FF66"
                        />
                    </View>

                    <Text>{l('additionalDetails')}</Text>
                    <TextInput
                        style={{ height: 40 }}
                        placeholder={l('additionalDetails')}
                        onChangeText={(text) => this.setState({ info: text })}
                    />

                    <View style={styles.buttons}>
                        <Button
                            title={l('cancel')}
                            onPress={this.toSellPage}
                        />
                        <Button
                            title={l('sell')}
                            onPress={this.sell}
                            color="#00FF66"
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    buttons: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    content: {
        padding: 10,
    },
    stars: {
        width: 50,
    },
    view: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: 'white',
        borderRadius: 10,
        color: 'red',
        padding: 15,
        margin: 45,
    }
});

function blobToDataURL(blob, callback) {
    var a = new FileReader();
    a.onload = function (e) { callback(e.target.result); }
    a.readAsDataURL(blob);
}