import React, { Component } from 'react';
import { View, TextInput, Button, StyleSheet, Text } from 'react-native';

import * as dbHandler from '../lib/dbHandler';
import App from '../App';
import l from '../lib/lang';

export default class CreateUser extends Component {
    constructor(props){
        super(props);
        this.state = {
            continue: false,
            phone: '',
            email: '',
            pass: '',
            name: '',
        }
        this.register = this.register.bind(this);
    }

    register() {
        dbHandler.CreateUser(this.state.email, this.state.pass, this.state.name, this.state.phone);
        this.setState({ continue: true });
    }

    render() {
        if (this.state.continue){
            return <App />
        }
        return (
            <View style={styles.view}>
                <Text>Syötä tietosi</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Nimi"
                    onChangeText={(text) => this.setState({ name: text })}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Puhelinnumero (muodossa +358)"
                    onChangeText={(text) => this.setState({ phone: text })}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Sähköpostiosoite"
                    onChangeText={(text) => this.setState({ email: text })}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Salasana"
                    onChangeText={(text) => this.setState({ pass: text })}
                    secureTextEntry={true}
                />
                <View style={styles.button}>
                    <Button
                        title="Jatka sovellukseen"
                        onPress={this.register}
                        color="#00FF66"
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        justifyContent: 'center',
        flex: 1
    },
    input: {
        padding: 10,
        height: 55,
    },
    button: {
        padding: 10,
    }
});