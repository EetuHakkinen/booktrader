import React from 'react';
import { View, StyleSheet, Alert, Text, Button, TextInput } from 'react-native';
import { SignOut } from '../lib/auth';
import LoadingSpinner from './LoadingSpinner';
import { UserInfo, UpdateUser } from '../lib/dbHandler';
import Facts from './Facts';

export default class Profile extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            email: '',
            phone: '',
            edit: false,
            facts: false,
            loading: true,
        }
    }

    componentDidMount() {
        UserInfo((n, p, e) => {
            this.setState({
                name: n,
                phone: p,
                email: e,
                loading: false,
            })
        });
    }

    render() {
        if (this.state.loading) {
            return <LoadingSpinner />
        }

        if (this.state.edit) {
            return <Edit name={this.state.name} phone={this.state.phone} email={this.state.email} />
        }

        if (this.state.facts) {
            return <Facts />
        }

        return (
            <View style={{ flex: 1 }}>
                <View style={styles.user}>
                    <Text style={styles.text}>{this.state.name}</Text>
                    <Text style={styles.text}>{this.state.phone}</Text>
                    <Text style={styles.text}>{this.state.email}</Text>
                    <Button
                        title="Muokkaa"
                        color="#00FF66"
                        onPress={() => this.setState({ edit: true })}
                    />
                </View>
                <View style={styles.bottom}>
                    <Button
                        title="Tietoja"
                        color="#00FF66"
                        onPress={() => this.setState({ facts: true })}
                    />
                    <Button
                        title="Kirjaudu ulos"
                        color="#00FF66"
                        onPress={() => SignOut()}
                    />
                </View>
            </View>
        );
    }
}

class Edit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.name,
            phone: this.props.phone,
            email: this.props.email,
            back: false,
        }
    }

    render() {
        if (this.state.back) {
            return <Profile />
        }

        return (
            <View style={{ flex: 1 }}>
                <View style={styles.user}>
                    <Text>Nimi</Text>
                    <TextInput
                        style={{ height: 40 }}
                        value={this.state.name}
                        onChangeText={(text) => this.setState({ name: text })}
                    />

                    <Text>Puhelinnumero</Text>
                    <TextInput
                        style={{ height: 40 }}
                        value={this.state.phone}
                        onChangeText={(text) => this.setState({ phone: text })}
                    />

                    <Text>Sähköpostiosoite</Text>
                    <TextInput
                        style={{ height: 40 }}
                        value={this.state.email}
                        onChangeText={(text) => this.setState({ email: text })}
                    />
                </View>
                <View style={styles.bottom}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', height: 5 }}>
                        <Button
                            title="OK"
                            color="#00FF66"
                            onPress={() => {
                                UpdateUser(this.state.name, this.state.phone, this.state.email);
                                this.setState({ back: true })
                            }}
                        />
                        <Button
                            title="Peruuta"
                            color="#00FF66"
                            onPress={() => {
                                this.setState({ back: true })
                            }}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    user: {
        justifyContent: 'center',
        flex: 1,
        padding: 10,
    },
    bottom: {
        padding: 5,
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 20,
    },
    text: {
        fontSize: 20,
        paddingLeft: 15,
    },
    card: {
        padding: 5,
    }
});