import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';

export default LoadingSpinner = () => {
    return (
        <View style={styles.view}>
            <ActivityIndicator size="large" color="#00FF66" />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: 20,
    }
});