import React from 'react';
import { View, TextInput, Button, StyleSheet, Alert } from 'react-native';
import firebase from 'react-native-firebase';

import App from '../App';
import * as dbHandler from '../lib/dbHandler';
import CreateUser from './CreateUser';

export default class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            email: '',
            pass: '',
            page: '',
        }
        this.signIn = this.signIn.bind(this);
    }

    signIn() {
        const { email, pass } = this.state;
        firebase.auth().signInWithEmailAndPassword(email, pass)
            .then((user) => {
                this.setState({ page: 'app' });
            })
            .catch(e => {
                Alert.alert(e.code)
            })
    }

    render() {
        if (this.state.page === 'create'){
            return <CreateUser />
        }

        if (this.state.page === 'app') {
            return <App />
        }

        return (
            <View style={styles.view}>
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => this.setState({ email: text })}
                    placeholder="Sähköpostiosoite"
                />
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => this.setState({ pass: text })}
                    placeholder="Salasana"
                    secureTextEntry={true}
                />
                <View style={styles.button}>
                    <Button
                        title="Luo käyttäjä"
                        onPress={() => this.setState({ page: 'create' })}
                        color="#00FF66"
                    />
                    <Button
                        title="Kirjaudu sisään"
                        onPress={this.signIn}
                        color='#00FF66'
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
    },
    input: {
        height: 55,
        padding: 10,
    },
    button: {
        justifyContent: 'space-between',
        padding: 10,
        flexDirection: 'row',
    }
});