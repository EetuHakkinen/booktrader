import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, FlatList, TouchableHighlight, ScrollView } from 'react-native';
import * as dbHandler from '../lib/dbHandler';
import firebase from 'react-native-firebase';
import l from '../lib/lang';
import StarRating from 'react-native-star-rating';
import { Card, CardTitle, CardAction } from 'react-native-material-cards';
import LoadingSpinner from './LoadingSpinner';

export default class Booklist extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { booklist, toBook, where } = this.props;
        if (booklist.length === 0) {
            return <LoadingSpinner />
        }
        return (
            <ScrollView style={styles.view}>
                <FlatList
                    data={booklist}
                    renderItem={({ item }) =>
                        <TouchableHighlight 
                            onPress={() => toBook(item.key)}
                            style={styles.card}
                            underlayColor="white">
                            <Card>
                                <CardTitle
                                    title={item.name}
                                    subtitle={item.price + ' €'}
                                />
                                <View style={{ width: 50, padding: 5 }}>
                                    <StarRating
                                        disabled={true}
                                        maxStars={5}
                                        rating={item.quality}
                                        starsize={10}
                                        fullStarColor="#00FF66"
                                    />
                                </View>
                            </Card>
                        </TouchableHighlight>}
                />
                <View style={styles.bottom}>

                </View>
            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    card: {
        padding: 5,
    },
    view: {
        minHeight: 500,
    },
    bottom: {
        height: 45
    }
});
