import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, Image, Linking, Dimensions, ScrollView, BackHandler } from 'react-native';
import { BookInformation, Remove } from '../lib/dbHandler';
import l from '../lib/lang';
import StarRating from 'react-native-star-rating';
import Home from './Home';
import Sell from './Sell';
import { DownloadImage } from '../lib/storage';
import { Uid } from '../lib/auth';

export default class BookStats extends Component {
    // props: bookId
    constructor(props) {
        super(props);
        this.state = {
            bookInfo: false,
            sellerName: '',
            number: '',
            email: '',
            toView: '',
            img: null,
            uid: '',
            sellerUid: '',
        }
        this.backToBooklist = this.backToBooklist.bind(this);
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        BookInformation(this.props.bookId, v => this.setState({ bookInfo: v }), v => this.setState({ sellerName: v }), v => this.setState({ number: v }), v => this.setState({ email: v }), u => this.setState({ sellerUid: u }));
        DownloadImage(this.props.bookId, v => this.setState({ img: v }));
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.setState({ toView: 'booklist' }); // works best when the goBack is async
            return true;
        });
        Uid().then((u) => this.setState({ uid: u }));
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    backToBooklist() {
        this.setState({ toView: 'booklist' });
    }

    remove() {
        Remove(this.props.bookId);
        this.setState({ toView: 'booklist' })
    }

    render() {
        var date = new Date(this.state.bookInfo.date);
        if (this.state.toView === 'booklist') {
            if (this.props.where === 'Home') {
                return <Home />
            } else if (this.props.where === 'Sell') {
                return <Sell />
            }
        }

        return (
            <ScrollView contentContainerStyle={styles.view}>
                <Text style={styles.bookName}>{this.state.bookInfo.name}</Text>
                <View>
                    {this.state.img ? <Image source={{ uri: this.state.img }} style={{ height: Dimensions.get('window').width, width: Dimensions.get('window').width }} /> : <View />}
                </View>
                <View style={styles.content}>
                    <Text>{l('published')} {date.getDate()}.{date.getMonth() + 1}.{date.getFullYear()}</Text>
                    <View style={styles.row}>
                        <StarRating
                            disabled={true}
                            maxStars={5}
                            rating={this.state.bookInfo.quality}
                            fullStarColor="#00FF66"
                            starsize={10}
                        />
                        <Text>{l('price')}: {this.state.bookInfo.price} €</Text>
                    </View>
                    <Text>{this.state.bookInfo.info}</Text>
                    {(this.state.uid === this.state.sellerUid)
                        ? <Button
                            title="Poista ilmoitus"
                            onPress={this.remove}
                        />
                        : <View>
                            <View style={styles.row}>
                                <Text>{this.state.sellerName}</Text>
                                <Text style={{ color: 'blue'}}
                                    onPress={() => {
                                        Linking.canOpenURL('whatsapp://send?text=&phone=' + this.state.number)
                                        .then(s => {
                                            if (s) {
                                                Linking.openURL('whatsapp://send?text=&phone=' + this.state.number)
                                            }
                                        })
                                    }}>
                                    {this.state.number}
                                </Text>
                            </View>
                            <Text style={{ color: 'blue' }}
                                onPress={() => Linking.openURL('mailto:' + this.state.email + '?subject=Booktrader: ' + this.state.bookInfo.name)}>
                                {this.state.email}
                            </Text>
                        </View>}

                    <Button
                        title={l('back')}
                        onPress={this.backToBooklist}
                        color="#00FF66"
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
    },
    content: {
        padding: 10,
    },
    bookName: {
        fontSize: 20,
        padding: 10,
    },
    row: {
        justifyContent: 'space-between',
        flexDirection: 'row',
    }
});