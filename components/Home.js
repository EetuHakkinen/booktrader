import React from 'react';
import { View, Text } from 'react-native';
import { ListenBookList } from '../lib/dbHandler';
import { SearchBar } from 'react-native-elements';
import Booklist from './Booklist';
import BookStats from './BookStats';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            booklist: [],
            toBook: '',
        }
    }

    componentWillMount() {
        if (this.props.booklist) {
            this.setState({ booklist: booklist });
        }
    }

    componentDidMount() {
        ListenBookList(v => this.setState({ booklist: v }));
    }

    render() {
        const filteredBooks = this.state.booklist.filter(b => b.name.includes(this.state.searchText));
        if (this.state.toBook !== ''){
            return <BookStats bookId={this.state.toBook} where="Home" />
        }
        return (
            <View>
                <SearchBar
                    lightTheme
                    platform="android"
                    cancelIcon={{ type: 'font-awesome', name: 'chevron-left' }}
                    placeholder='Search'
                    onChangeText={(text) => this.setState({ searchText: text })}
                    onClear={() => this.setState({ searchText: '' })}
                />
                <Booklist booklist={filteredBooks} toBook={b => this.setState({ toBook: b })} />
            </View>
        );
    }
}