import React from 'react';
import { View, FlatList, Text, Button, Linking, TouchableHighlight, BackHandler } from 'react-native';
import { Card, CardTitle, CardAction, CardContent } from 'react-native-material-cards';
import Profile from './Profile';

const libraries = [
    {
        key: 0,
        name: 'React',
        by: 'Facebook',
        description: 'A declarative, efficient, and flexible JavaScript library for building user interfaces.\nLicence: MIT',
        url: 'https://reactjs.org/'
    },
    {
        key: 1,
        name: 'React native',
        by: 'Facebook',
        description: 'React Native lets you build mobile apps using only JavaScript. It uses the same design as React, letting you compose a rich mobile UI using declarative components.\nLicence: MIT',
        url: 'https://facebook.github.io/react-native/'
    },
    {
        key: 2,
        name: 'React Native Firebase',
        by: 'Invertase',
        description: 'A well tested feature rich modular Firebase implementation for React Native. Supports both iOS & Android platforms for over 15 Firebase services.\nLicence: Apache-2.0',
        url: 'https://rnfirebase.io/'
    },
    {
        key: 3,
        name: 'React-Native-Material-Cards',
        by: 'SiDevesh',
        description: 'A material design card component, customizable and versatile.\nLicence: MIT',
        url: 'https://github.com/SiDevesh/React-Native-Material-Cards'
    },
    {
        key: 4,
        name: 'React Native Star Rating Component',
        by: 'djchie',
        description: 'A React Native component for generating and displaying interactive star ratings.\nLicence: ISC',
        url: 'https://github.com/djchie/react-native-star-rating'
    },
    {
        key: 5,
        name: 'React Native Elements',
        by: 'react-native-training',
        description: 'Cross Platform React Native UI Toolkit\nLicence: MIT',
        url: 'https://github.com/react-native-training/react-native-elements'
    },
    {
        key: 6,
        name: 'React Native Image Crop Picker',
        by: 'Ivpusic',
        description: 'iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping\nLicence: MIT',
        url: 'https://github.com/ivpusic/react-native-image-crop-picker'
    },
    {
        key: 7,
        name: 'rn-fetch-blob',
        by: 'Joltup',
        description: 'A project committed to making file access and data transfer easier, efficient for React Native developers.\nLicence: MIT',
        url: 'https://github.com/joltup/rn-fetch-blob'
    }
]

export default class Facts extends React.Component {
    constructor() {
        super();
        this.state = {
            back: false
        }
    }

    componentWillMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.setState({ back: true }); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    render() {
        if (this.state.back) {
            return <Profile />
        }

        return (
            <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 25, paddingLeft: 10 }}>Libraries</Text>
                <FlatList
                    data={libraries}
                    renderItem={({ item }) =>
                        <TouchableHighlight
                            onPress={() => Linking.openURL(item.url)}
                            style={{ padding: 5 }}
                            underlayColor="white">
                            <Card>
                                <CardTitle
                                    title={item.name}
                                    subtitle={item.by}
                                />
                                <CardContent text={item.description} />
                            </Card>
                        </TouchableHighlight>}
                />
                <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 35 }}>
                    <Button 
                        title="Takaisin"
                        color="#00FF66"
                        onPress={() => this.setState({ back: true })}
                    />
                </View>
            </View>
        )
    }
}
