export default {
    //Booklist
    'books': 'Kirjat:',

    //Sell-book-page
    'nameOfBook': 'Kirjan nimi',
    'price': 'Hinta (€)',
    'quality': 'Kunto',
    'additionalDetails': 'Lisätietoja',
    'sell': 'Myy',
    'cancel': 'Peruuta',
    'sellBook': 'Myy kirja',

    // UserInit
    'continue': 'Jatka sovellukseen',
    'phone': 'Puhelinnumero',
    'email': 'Sähköpostiosoite',

    //BookStats
    'published': 'ilmoitettu',
    'back': 'takaisin',
};